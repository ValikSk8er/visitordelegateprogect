﻿using System;
using VisitorHomeTask.DataLoggerTypes;
using VisitorHomeTask.ErrorTypes;

namespace VisitorHomeTask
{
    class Program
    {  
        static void Main(string[] args)
        {
            var errorHandler = new ErrorHandler();            

            var amazonServerError = new ServerErrorInfo()
            {
                Description = "Amazon",
                Severity = Severity.Critical,
                TimeStamp = DateTime.Today,
                Id = Guid.NewGuid(),
                ServerId = Guid.NewGuid()
            };

            var microsoftServerError = new ServerErrorInfo()
            {
                Description = "Microsoft",
                Severity = Severity.High,
                TimeStamp = DateTime.Today,
                Id = Guid.NewGuid(),
                ServerId = Guid.NewGuid()
            };

            var googleServerError = new ServerErrorInfo()
            {
                Description = "Google",
                Severity = Severity.Low,
                TimeStamp = DateTime.Today,
                Id = Guid.NewGuid(),
                ServerId = Guid.NewGuid()
            };

            var amazonDatabaseError = new DatabaseErrorInfo()
            {
                Description = "Amazon",
                Severity = Severity.Critical, 
                TimeStamp = DateTime.Today,
                DatabaseName = "Servers",
                Id = Guid.NewGuid(),
                UserId = Guid.NewGuid()
            };

            var microsoftDatabaseError = new DatabaseErrorInfo()
            {
                Description = "Microsoft",
                Severity = Severity.High,
                TimeStamp = DateTime.Today,
                DatabaseName = "Licenses",
                Id = Guid.NewGuid(),
                UserId = Guid.NewGuid()
            };

            var googleDatabaseError = new DatabaseErrorInfo()
            {   Severity = Severity.Low,
                Description = "Google",
                TimeStamp = DateTime.Today,
                DatabaseName = "Users",
                Id = Guid.NewGuid(),
                UserId = Guid.NewGuid()
            };

            errorHandler.Add(amazonServerError);
            errorHandler.Add(microsoftServerError);
            errorHandler.Add(googleServerError);

            errorHandler.Add(amazonDatabaseError);
            errorHandler.Add(microsoftDatabaseError);
            errorHandler.Add(googleDatabaseError);

            var visitorShowDataInRow = new Visitor();
            visitorShowDataInRow.LogServerInfo = (ServerErrorInfo errorInfo) => LogDataInRow.LogServerInfo(errorInfo);
            visitorShowDataInRow.LogDatabaseInfo = (DatabaseErrorInfo errorInfo) => LogDataInRow.LogDatabaseInfo(errorInfo);

            var visitorShowDataInColumn = new Visitor();
            visitorShowDataInColumn.LogServerInfo = (ServerErrorInfo errorInfo) => LogDataInColumn.LogServerInfo(errorInfo);
            visitorShowDataInColumn.LogDatabaseInfo = (DatabaseErrorInfo errorInfo) => LogDataInColumn.LogDatabaseInfo(errorInfo);

            errorHandler.Accept(visitorShowDataInColumn);
            errorHandler.Accept(visitorShowDataInRow);

            Console.ReadKey();
        }
    }
}
