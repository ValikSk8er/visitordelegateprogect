﻿using System;
using VisitorHomeTask.ErrorTypes;

namespace VisitorHomeTask
{
    public class Visitor
    {
        public Action<ServerErrorInfo> LogServerInfo;
        public Action<DatabaseErrorInfo> LogDatabaseInfo;
    }
}
