﻿using System;

namespace VisitorHomeTask.ErrorTypes
{
    public class ServerErrorInfo : ErrorInfo
    {
        public Guid ServerId { get; set; }

        public override void Accept(Visitor visitor) => visitor.LogServerInfo(this);
    }
}
