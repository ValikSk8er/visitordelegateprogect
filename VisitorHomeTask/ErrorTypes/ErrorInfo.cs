﻿using System;

namespace VisitorHomeTask.ErrorTypes
{
    public abstract class ErrorInfo
    {
        public string Description { get; set; }
        public Guid Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public Severity Severity { get; set; }

        public abstract void Accept(Visitor visitor);
    }

    public enum Severity
    {
        Low,
        High,
        Critical
    }
}
