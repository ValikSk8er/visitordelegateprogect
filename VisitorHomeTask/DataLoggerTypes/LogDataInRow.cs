﻿using System;
using VisitorHomeTask.ErrorTypes;

namespace VisitorHomeTask.DataLoggerTypes
{
    public static class LogDataInRow
    {
        public static void LogDatabaseInfo(DatabaseErrorInfo errorInfo)
        {
            Console.WriteLine(new string('-', 100));
            Console.WriteLine($" Log Database Info:");
            Console.Write($" Description : {errorInfo.Description};");
            Console.Write($" Id : {errorInfo.Id};");
            Console.WriteLine($" TimeStamp : {errorInfo.TimeStamp};");
            Console.Write($" Severity : {errorInfo.Severity};");
            Console.Write($" DatabaseName : {errorInfo.DatabaseName};");
            Console.WriteLine($" UserId : {errorInfo.UserId}");
        }

        public static void LogServerInfo(ServerErrorInfo errorInfo)
        {
            if (errorInfo.Severity == Severity.Critical)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            Console.WriteLine(new string('-', 100));
            Console.WriteLine($" Log Server Info:");
            Console.Write($" Description : {errorInfo.Description};");
            Console.Write($" Id : {errorInfo.Id};");
            Console.WriteLine($" TimeStamp : {errorInfo.TimeStamp};");
            Console.Write($" Severity : {errorInfo.Severity};");
            Console.WriteLine($" ServerId : {errorInfo.ServerId}");

            
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
