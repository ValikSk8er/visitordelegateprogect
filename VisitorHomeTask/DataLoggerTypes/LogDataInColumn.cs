﻿using System;
using VisitorHomeTask.ErrorTypes;

namespace VisitorHomeTask.DataLoggerTypes
{
    public static class LogDataInColumn
    {
        public static void LogServerInfo(ServerErrorInfo errorInfo)
        {
            if (errorInfo.Severity == Severity.Critical)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            Console.WriteLine(new string('-', 55));
            Console.WriteLine($" Log Server Info:");
            Console.WriteLine($" Description  : {errorInfo.Description}");
            Console.WriteLine($" Id           : {errorInfo.Id}");
            Console.WriteLine($" TimeStamp    : {errorInfo.TimeStamp}");
            Console.WriteLine($" ServerId     : {errorInfo.ServerId}");
            Console.WriteLine($" Severity     : {errorInfo.Severity}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void LogDatabaseInfo(DatabaseErrorInfo errorInfo)
        {
            Console.WriteLine(new string('-', 55));
            Console.WriteLine($" Log Database Info:");
            Console.WriteLine($" Description  : {errorInfo.Description}");
            Console.WriteLine($" Id           : {errorInfo.Id}");
            Console.WriteLine($" TimeStamp    : {errorInfo.TimeStamp}");
            Console.WriteLine($" DatabaseName : {errorInfo.DatabaseName}");
            Console.WriteLine($" UserId       : {errorInfo.UserId}");
            Console.WriteLine($" Severity     : {errorInfo.Severity}");
        }
    }
}
